// ==UserScript==
// @name         twNightModeRevealPoll
// @namespace    https://gitlab.com/hrnry/userscript
// @version      0.1
// @description  Twitter: Enable NightMode and Reveal Poll results w/o voting
// @author       hrnry
// @include      /https?:\/\/(.*\.)?twitter\.com\/.*/
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  //Night Mode
  document.querySelectorAll("link.coreCSSBundles").forEach(link => {
    link.href = link.href.replace(/\/twitter_core.bundle.css/, "/nightmode_twitter_core.bundle.css");
  });
  document.querySelectorAll("link.moreCSSBundles").forEach(link => {
    link.href = link.href.replace(/\/twitter_more_/, "/nightmode_twitter_more_");
  });
  document.querySelector('div.TwitterCard').setAttribute("data-theme", "night-mode");

  //Reveal Poll results without voting
  document.querySelectorAll('div.PollXChoice-optionContainer > label.PollXChoice-choice > span.PollXChoice-choice--text > span.PollXChoice-progress').forEach(elm => {
    elm.style.overflow = "visible";
    elm.style.marginLeft = 0;
  });
})();