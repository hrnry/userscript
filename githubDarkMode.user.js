// ==UserScript==
// @name         githubDarkMode
// @namespace    https://gitlab.com/hrnry/userscript
// @version      0.1
// @description  GitHub Dark mode
// @author       hrnry
// @match        https://github.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // New from Universe 2020: Dark mode, GitHub Sponsors for companies, and more
    // https://github.blog/2020-12-08-new-from-universe-2020-dark-mode-github-sponsors-for-companies-and-more/
    document.documentElement.setAttribute("data-color-mode", "dark");
    document.documentElement.setAttribute("data-light-theme", "light");
    document.documentElement.setAttribute("data-dark-theme", "dark");
    document.querySelector("header").style.backgroundColor = "#0f0903";
})();
